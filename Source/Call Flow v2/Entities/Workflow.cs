﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml.Linq;

namespace Call_Flow_v2.Entities
{
    [Serializable]
    public class Workflow
    {
        #region Constructor

        /// <summary>
        /// Creates a new instance of the <see cref="Workflow"/> class.
        /// </summary>
        public Workflow(XElement node)
        {
            // Store
            this.Name = node.Attribute("name").Value;

            // Get buttons
            List<WorkflowButton> buttons = new List<WorkflowButton>();
            foreach (XElement button in node.Element("Buttons").Elements("Button"))
            {
                // Get brush
                SolidColorBrush brush = new BrushConverter().ConvertFromString(button.Attribute("color").Value) as SolidColorBrush;

                // Add
                buttons.Add(new WorkflowButton(button.Attribute("text").Value, brush, button.Attribute("nextWorkflowName").Value));
            }

            this.Buttons = buttons.ToArray();

            // Content replacements
            string content = node.Element("Content").Value;
            content = content.Replace("{UserName}", $"{Settings.FirstName} {Settings.LastName}");
            content= content.Replace("{HotelName}", Settings.HotelName);

            // Set content
            this.Content = content;
        }

        #endregion

        #region Public

        /// <summary>
        /// Gets or sets the name of the workflow.
        /// </summary>
        public string Name
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the buttons that shoul appear in the workflow.
        /// </summary>
        public WorkflowButton[] Buttons
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the content text.
        /// </summary>
        public string Content
        {
            get; set;
        }

        #endregion
    }
}
