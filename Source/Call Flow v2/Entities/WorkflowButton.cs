﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Call_Flow_v2.Entities
{
    [Serializable]
    public class WorkflowButton
    {
        #region Constructor

        /// <summary>
        /// Creates a new instance of the <see cref="WorkflowButton"/> class.
        /// </summary>
        /// <param name="text">The button text</param>
        /// <param name="color">The button color.</param>
        /// <param name="nextWorkflowName">The name of the next workflow.</param>
        public WorkflowButton(string text, Brush color, string nextWorkflowName)
        {
            // Set
            this.Text = text;
            this.Color = color;
            this.NextWorkflowName = nextWorkflowName;
        }

        #endregion

        #region Public

        public string Text
        {
            get;set;
        }

        public Brush Color
        {
            get;set;
        }

        public string NextWorkflowName
        {
            get;set;
        }

        #endregion
    }
}
