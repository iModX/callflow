﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Call_Flow_v2
{
    public static class Logging
    {
        private static bool IsInitialized = false;
        public static event EventHandler LogEvent;

        public static void Initialize(string filePath)
        {
            if (!IsInitialized)
            {
                string logDirectory = Path.GetDirectoryName(filePath);

                // Directory does not exists?
                if (!Directory.Exists(logDirectory))
                {
                    // Create directory
                    Directory.CreateDirectory(logDirectory);
                }

                // Create file
                FileStream logFileStream = File.Create(filePath);

                //Create a new text writer using the output stream, and add it to * the trace listeners.
                TextWriterTraceListener myTextListener = new TextWriterTraceListener(logFileStream);

                // Add
                Trace.Listeners.Add(myTextListener);

                // Set settings
                Trace.AutoFlush = true;
            }

            // Set
            IsInitialized = true;
        }

        public static void Write(Exception exception)
        {
            Write(exception.ToString(), TraceLevel.Error);
        }

        public static void Write(string message, TraceLevel traceLevel)
        {
            Trace.WriteLine(message, $"{DateTime.Now} [{traceLevel}]");

            // Raise event
            LogEvent?.Invoke($"{DateTime.Now} [{traceLevel}] {message}", EventArgs.Empty);
        }
    }
}
