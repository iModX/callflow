﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Call_Flow_v2
{
    public static class Settings
    {
        public static string FirstName = "Your firstname";
        public static string LastName = "Your lastname";
        public static string HotelName = "Your hotelname";
    }
}
