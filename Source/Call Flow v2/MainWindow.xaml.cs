﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Xml.Linq;

using Call_Flow_v2.Entities;

namespace Call_Flow_v2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IEnumerable<Workflow> _workflows;
        ContentManager _contentManager;

        #region Constructor

        /// <summary>
        /// Creates a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            // Initialize
            InitializeComponent();

            // Datacontext
            this.RootGrid.DataContext = this;
        }

        #endregion

        #region Private

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                // File path
                string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Workflows.xml");

                // Not found?
                if (!File.Exists(filePath))
                {
                    throw new FileNotFoundException($"Could not find \"{filePath}\"", Path.GetFileName(filePath));
                }

                // Load workflows
                this._workflows = this.LoadWorkflows(filePath);

                // No workflows found?
                if (!this._workflows.Any())
                {
                    throw new Exception("Please describe one or more workflows first.");
                }

                // Create content manager
                this._contentManager = new ContentManager(this);

                // Show settings
                this._contentManager.ShowSettings();

                // Init log
                Logging.Initialize(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log", $"{Settings.FirstName}{Settings.LastName}.log"));
                Logging.Write($"Logged in as \"{Settings.FirstName} {Settings.LastName}\".", TraceLevel.Info);
                Logging.Write($"Hotel name: {Settings.HotelName}", TraceLevel.Info);

                // Set content
                this._contentManager.SetContent(this._workflows.First());
            }
            catch (Exception exception)
            {
                // Handle
                MessageBox.Show(exception.ToString());
            }
        }

        private IEnumerable<Workflow> LoadWorkflows(string filePath)
        {
            // Load xml
            XElement root = XDocument.Load(filePath).Element("Workflows");

            // Process workflows
            List<Workflow> workflows = new List<Workflow>();
            foreach (XElement workflowNode in root.Elements("Workflow"))
            {
                // Add
                workflows.Add(new Workflow(workflowNode));
            }

            // Return
            return workflows;
        }

        private void ButtonCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                // Gets next workflow name
                string nextWorkflowName = e.Parameter as string;

                if (nextWorkflowName.ToLower() == "settings")
                {
                    // Set content
                    this._contentManager.ShowSettings();
                    return;
                }

                // Get workkflow by name
                Workflow workflow = this._workflows.SingleOrDefault(w => w.Name == nextWorkflowName);

                if (workflow == null)
                {
                    MessageBox.Show($"Unable to find workflow \"{nextWorkflowName}\"");
                    return;
                }

                // Set content
                this._contentManager.SetContent(workflow);

                // Log
                Logging.Write($"Workflow \"{nextWorkflowName}\" selected.", TraceLevel.Info);
            }
            catch (Exception exception)
            {
                // Handle
                MessageBox.Show(exception.ToString());
            }
        }

        #endregion
    }
}
