﻿using Call_Flow_v2.Entities;
using Call_Flow_v2.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Call_Flow_v2
{
    public class ContentManager
    {
        private MainWindow _mainWindow;

        #region Constructor

        /// <summary>
        /// Creates a new instance of the <see cref="ContentManager"/> class.
        /// </summary>
        public ContentManager(MainWindow mainWindow)
        {
            // Store
            this._mainWindow = mainWindow;
        }

        #endregion

        #region Public

        /// <summary>
        /// Sets the given control as content.
        /// </summary>
        public void SetContent(Workflow workflow)
        {
            this._mainWindow.ContentPlaceholder.Content = new WorkflowUserControl(workflow);
        }

        /// <summary>
        /// Shows the settings window.
        /// </summary>
        public void ShowSettings()
        {
            new SettingsWindow().ShowDialog();
        }

        #endregion
    }
}
