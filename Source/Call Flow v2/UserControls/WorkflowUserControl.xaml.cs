﻿using Call_Flow_v2.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Call_Flow_v2.UserControls
{
    /// <summary>
    /// Interaction logic for WorkflowUserControl.xaml
    /// </summary>
    public partial class WorkflowUserControl : UserControl
    {
        private Workflow _workflow;

        #region Constructor

        /// <summary>
        /// Creates a new instance of the <see cref="WorkflowUserControl"/> class.
        /// </summary>
        /// <param name="workflow">The workflow.</param>
        public WorkflowUserControl(Workflow workflow)
        {
            // Initialize
            InitializeComponent();

            // Create navigation
            foreach (WorkflowButton button in workflow.Buttons)
            {
                // Create control
                RoundButtonUserControl buttonControl = new RoundButtonUserControl();
                buttonControl.Text = button.Text;
                buttonControl.Color = button.Color;
                buttonControl.NextWorkflowName = button.NextWorkflowName;

                // Add button
                this.Navigation.Children.Add(buttonControl);
            }

            // Store
            this._workflow = workflow;

            // Datacontext
            this.RootGrid.DataContext = this;
        }

        #endregion

        #region Public


        /// <summary>
        /// Gets the workflow.
        /// </summary>
        public Workflow Workflow
        {
            get => this._workflow;
        }

        #endregion
    }
}
