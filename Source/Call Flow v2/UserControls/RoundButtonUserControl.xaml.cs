﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Call_Flow_v2.UserControls
{
    /// <summary>
    /// Interaction logic for RoundButtonUserControl.xaml
    /// </summary>
    public partial class RoundButtonUserControl : UserControl
    {
        public RoundButtonUserControl()
        {
            // Initialize
            InitializeComponent();

            // Datacontext
            this.RootGrid.DataContext = this;
        }

        #region Public

        /// <summary>
        /// Gets or sets the button text
        /// </summary>
        public string Text
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the button color.
        /// </summary>
        public Brush Color
        {
            get;
            set;
        }

        public string NextWorkflowName
        {
            get;set;
        }

        #endregion
    }
}
